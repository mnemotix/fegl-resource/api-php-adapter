<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter;

use \DateTime;
use ReSourceAdapter\Helpers\IdHelper;
use ReSourceAdapter\Helpers\ListQueryParams;
use ReSourceAdapter\Model\Actor;
use ReSourceAdapter\Model\Artwork;
use ReSourceAdapter\Model\Event;
use ReSourceAdapter\Model\Memo;
use ReSourceAdapter\Model\Exhibition;
use ReSourceAdapter\Model\Project;
use ReSourceAdapter\Model\Resource;
use ReSourceAdapter\Model\ResourceActivity;
use ReSourceAdapter\Resolver\Client;

class Adapter{

  protected $lang = "fr";

  protected $client;

  /**
   * Adapter constructor.
   * @param string $lang Requested lang
   * @param string $graphQLEndpointURI Re-Source Endpoint URI
   */
  public function __construct($lang = 'fr') {
    $this->lang = $lang;
    $this->client = new Client();
  }

  /**
   * Set language context.
   * @param $lang
   */
  public function setLang($lang){
    $this->lang = $lang;
  }

  /**
   * Set a client
   * @param $client
   */
  public function setClient($client){
    $this->client = $client;
  }

  /**
   * Find actors list
   *
   * @param string $qs Query string
   * @param int $first Number of returned actors
   * @param int $after Offset of the first returned actor. Default to null.
   *
   * @return \ReSourceAdapter\Model\Actor[]
   */
  public function findActors($qs, $first, $after = null){
    $args = ListQueryParams::fromArray([
      'qs' => $qs,
      'first' => $first,
      'after' => $after
    ]);

    $query = Actor::getListQuery($args);

    $response  = $this->client->resolveQuery($query, $this->lang);

    return Actor::fromListResponse($response);
  }

  /**
   * Find exhibitions list.
   *
   * @param string $qs Query string
   * @param int $first Number of returned exhibitions
   * @param int $after Offset of the first return exhibition. Default to null.
   *
   * @return \ReSourceAdapter\Model\Exhibition[]
   */
  public function findExhibitions($qs, $first, $after = null){
    $args = ListQueryParams::fromArray([
      'qs' => $qs,
      'first' => $first,
      'after' => $after
    ]);

    $query = Exhibition::getListQuery($args);

    $response  = $this->client->resolveQuery($query, $this->lang);

    return Exhibition::fromListResponse($response);
  }

  /**
   * Find artworks list for an exhibition.
   *
   * @param string $exhibitionId Exhibition id
   * @param string $qs Query string
   * @param int $first Number of returned artworks
   * @param int $after Offset of the first returned artwork. Default to null.
   *
   * @return \ReSourceAdapter\Model\Artwork[]
   */
  public function findExhibitionArtworks($exhibitionId, $qs, $first, $after = null){
    $args = ListQueryParams::fromArray([
      'qs' => $qs,
      'first' => $first,
      'after' => $after
    ]);

    $query = Artwork::getListQuery($this->normalizeId($exhibitionId), $args);

    $response  = $this->client->resolveQuery($query, $this->lang);

    return Artwork::fromListResponse($response);
  }

  /**
   * Find events list for an exhibition.
   *
   * @param string $exhibitionId Exhibition id
   * @param string $qs Query string
   * @param int $first Number of returned artworks
   * @param int $after Offset of the first returned artwork. Default to null.
   *
   * @return \ReSourceAdapter\Model\Event[]
   */
  public function findExhibitionEvents($exhibitionId, $qs, $first, $after = null){
    $args = ListQueryParams::fromArray([
      'qs' => $qs,
      'first' => $first,
      'after' => $after
    ]);

    $query = Event::getListQuery($this->normalizeId($exhibitionId), $args);

    $response  = $this->client->resolveQuery($query, $this->lang);


    return Event::fromListResponse($response);
  }

  /**
   * Find events list for an artwork.
   *
   * @param string $artworkId Exhibition id
   * @param string $qs Query string
   * @param int $first Number of returned artworks
   * @param int $after Offset of the first returned artwork. Default to null.
   *
   * @return \ReSourceAdapter\Model\Event[]
   */
  public function findArtworkEvents($artworkId, $qs, $first, $after = null){
    return $this->findExhibitionEvents($artworkId, $qs, $first, $after);
  }

  /**
   * Get actor by id.
   *
   * @param string $id Actor id
   *
   * @return \ReSourceAdapter\Model\Actor
   */
  public function getActor($id){
    $query = Actor::getQuery($this->normalizeId($id));

    $response  = $this->client->resolveQuery($query, $this->lang);

    return Actor::fromResponse($response);
  }

  /**
   * Get exhibition by id.
   *
   * @param string $id Exhibition id
   *
   * @return \ReSourceAdapter\Model\Exhibition
   */
  public function getExhibition($id){
    $query = Exhibition::getQuery($this->normalizeId($id));

    $response  = $this->client->resolveQuery($query, $this->lang);

    return Exhibition::fromResponse($response);
  }

  /**
   * Get artwork by id.
   *
   * @param string $id Artwork id
   *
   * @return \ReSourceAdapter\Model\Artwork
   */
  public function getArtwork($id){
    $query = Artwork::getQuery($this->normalizeId($id));
    $response  = $this->client->resolveQuery($query, $this->lang);

    return Artwork::fromResponse($response);
  }

  /**
   * Get event by id.
   *
   * @param string $id Event id
   *
   * @return \ReSourceAdapter\Model\Event
   */
  public function getEvent($id){
    $query = Event::getQuery($this->normalizeId($id));

    $response  = $this->client->resolveQuery($query, $this->lang);

    return Event::fromResponse($response);
  }


  /**
   * Get resource by id.
   *
   * @param string $id Resource id
   *
   * @return \ReSourceAdapter\Model\Resource
   */
  public function getResource($id){
    $query = Resource::getQuery($this->normalizeId($id));

    $response  = $this->client->resolveQuery($query, $this->lang);

    return Resource::fromResponse($response);
  }

  /**
   * Get resource by id.
   *
   * @param string $id Resource id
   *
   * @return \ReSourceAdapter\Model\Memo
   */
  public function getMemo($id){
    $query = Memo::getQuery($this->normalizeId($id));

    $response  = $this->client->resolveQuery($query, $this->lang);

    return Memo::fromResponse($response);
  }

  /**
   * Get resources differential since.
   *
   * @param float $date Timestamp from which resources have been modified.
   * @param int $first Number of returned resource activity objects
   * @param int $after Offset of the first returned resource activity object. Default to null.
   *
   * @return \ReSourceAdapter\Model\ResourceActivity[]
   */
  public function getResourcesDifferentialSince($date, $first = 10, $after = null) {
    $isoDate = date(DateTime::ISO8601, $date);

    $args = ListQueryParams::fromArray([
      'filters' => ["updatedAt >= $isoDate"],
      'first' => $first,
      'after' => $after
    ]);

    $query = ResourceActivity::getListQuery($args);

    $response  = $this->client->resolveQuery($query, $this->lang);

    return ResourceActivity::fromListResponse($response);
  }

  private function normalizeId($id){
    return IdHelper::toNewId($id);
  }
}

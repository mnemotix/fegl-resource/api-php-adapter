<?php
/**
 * Created by IntelliJ IDEA.
 * User: mrogelja
 * Date: 13/04/2017
 * Time: 10:17
 */

namespace ReSourceAdapter\Resolver;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use PHPUnit\Runner\Exception;

/**
 * Class Client
 * @package ReSourceAdapter\Resolver
 */
class Client {

  protected $httpClient;

  protected $graphQLEndpointURI = "https://prod-resource-weever.mnemotix.com/graphql";

  private $oauthTokenEndpointURI = "https://prod-resource-auth.mnemotix.com/auth/realms/synaptix/protocol/openid-connect/token";
  private $oauthUsername;
  private $oauthPassword;
  private $oauthClientId;
  private $oauthClientSecret;
  private $oauthAccessToken;
  private $oauthRefreshToken;

  /**
   * Client constructor.
   *
   * @param $graphQLEndpointURI
   * @throws \Exception
   */
  public function __construct() {
    $endpointURI = getenv('RESOURCE_ENDPOINT_URI');

    if (false !== $endpointURI){
      $this->graphQLEndpointURI = $endpointURI;
    }

    $authURI = getenv('RESOURCE_AUTH_URI');

    if (false !== $authURI){
      $this->oauthTokenEndpointURI = $authURI;
    }

    $apiKey = getenv('RESOURCE_API_KEY');

//     if (false !== $apiKey) {
//       $this->parseApiKey($apiKey);
//     } else {
//       throw new \Exception(<<<MESSAGE
// You must provide an API key by setting the RESOURCE_ENDPOINT_URI environment variable.
// MESSAGE
//       );
//     }

    $this->httpClient = new \GuzzleHttp\Client([
      'base_uri' => $this->graphQLEndpointURI,
      'timeout'  => 600.0,
    ]);
  }

  /**
   * Init connection settings with API key.
   * @param $key
   */
  protected function parseApiKey($key){
    $settings = explode('::', base64_decode($key));

    if (count($settings) !== 4) {
      throw new Exception(<<<MESSAGE
Your API key provided in RESOURCE_API_KEY environment variable is not valid.
MESSAGE
);
    }

    $this->oauthUsername  =  $settings[2];
    $this->oauthPassword  = $settings[3];
    $this->oauthClientId  =  $settings[0];
    $this->oauthClientSecret = $settings[1];
  }

  public function refreshToken()
  {
    $oautHttpClient = new \GuzzleHttp\Client([
      'base_uri' => $this->oauthTokenEndpointURI,
      'timeout'  => 30.0
    ]);

    $response = $oautHttpClient->request('POST', '', [
      'form_params' => [
        'username' => $this->oauthUsername,
        'password' => $this->oauthPassword,
        'client_id' => $this->oauthClientId,
        'client_secret' => $this->oauthClientSecret,
        'grant_type' => 'password'
      ],
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded'
      ]
    ]);

    $response = json_decode((string) $response->getBody(), JSON_OBJECT_AS_ARRAY);

    $this->oauthAccessToken = $response['access_token'];
    $this->oauthRefreshToken = $response['refresh_token'];
  }

  /**
   * @return mixed
   */
  public function getOauthAccessToken() {
    return $this->oauthAccessToken;
  }

  /**
   * @return mixed
   */
  public function getOauthRefreshToken() {
    return $this->oauthRefreshToken;
  }

  /**
   * Send a GraphQL query.
   * @param $query
   * @param string $lang
   * @return mixed
   * @throws \ReSourceAdapter\Resolver\GraphQLResponseError
   */
  public function resolveQuery($query, $lang = "fr", $retry = 0){
//     $this->refreshToken();

    $graphQLEndpoint = parse_url($this->graphQLEndpointURI);

//     $cookies = CookieJar::fromArray([
//       'access_token' => $this->oauthAccessToken,
//       'refresh_token' => $this->oauthRefreshToken,
//     ], $graphQLEndpoint['host']);

    try {
      //echo $query;

      $response = $this->httpClient->request('POST', '', [
        'json' => ['query' => $query],
//         'cookies' => $cookies,
        'headers' => [
          'Lang' => $lang
        ]
      ]);

      $data = json_decode((string) $response->getBody(), JSON_OBJECT_AS_ARRAY);

      return $data['data'];

    } catch (ConnectException $e) {
      if($retry < 3){
        return $this->resolveQuery($query, $lang, $retry+1);
      } else {
        throw new \Exception("Timeout error on GraphQL query :\n $query");
      }
    } catch (Exception $e) {
      if ($e->getCode() == 400) {
        $data = json_decode((string) $e->getResponse()->getBody(), JSON_OBJECT_AS_ARRAY);

        throw new GraphQLResponseError($data['errors']);
      } else {

        //throw $e;
      }
    }
  }

  public function ping(){
    return $this->resolveQuery(<<<GRAPHQL
query {
  environment{
    __typename
	}
}
GRAPHQL
    );
  }
}

<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

class Person extends Actor {
  /** @var string Person first name */
  protected $firstName;

  /** @var string Person last name */
  protected $lastName;

  /** @var string Person gender */
  protected $gender;

  /** @var string Person biography */
  protected $bio;

  /** @var string Person short biography */
  protected $shortBio;

  /** @var string Person birthday */
  protected $birthday;

  /** @var string Locality place of birth */
  protected $placeOfBirth;

  /** @var string Locality place of work */
  protected $placeOfWork;

  /**
   * @return string
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * @return string
   */
  public function getLastName() {
    return $this->lastName;
  }

  /**
   * @return string
   */
  public function getGender() {
    return $this->gender;
  }

  /**
   * @return string
   */
  public function getBio() {
    return $this->bio;
  }

  /**
   * @return string
   */
  public function getShortBio() {
    return $this->shortBio;
  }

  /**
   * @return string
   */
  public function getBirthday() {
    if ( $this->birthday ){
      return date("U000",strtotime($this->birthday));
    }
  }

  /**
   * @return string
   */
  public function getPlaceOfBirth() {
    return $this->placeOfBirth;
  }

  /**
   * @return string
   */
  public function getPlaceOfWork() {
    return $this->placeOfWork;
  }

  /**
   * Get person GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    return <<<GRAPHQL
fragment $fragmentName on Person{
  id
  displayName
  avatar
  fullName
  firstName
  lastName
  bio
  shortBio
  gender
  seeAlso 
  creationDate: createdAt
  lastUpdate: updatedAt
  birthday
  externalLinks{
    edges{
      externalLink: node{
        name: label
        link
      }
    }
  }
  addresses {
    edges {
      locality: node {
        id
        name: label
        street1
        street2
        postCode: postalCode
        city
        countryName: country
      }
    }
  }
}
GRAPHQL;
  }

  /**
   * Get person from GraphQL response data.
   * @param $data
   * @return \ReSourceAdapter\Model\Person
   */
  static function fromResponse($data) {
    $data = $data['person'];

    $person = new Person();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'externalLinks':
          if(isset($value)) {
            foreach ($value['edges'] as $externalLinkNode){
              $externalLink = ExternalLink::fromResponse($externalLinkNode);

              switch ($externalLink->getName()) {
                case 'facebook':
                  $person->facebookUri = $externalLink->getLink();
                  break;
                case 'twitter':
                  $person->twitterUri = $externalLink->getLink();
                  break;
                case 'instagram':
                  $person->instagramUri = $externalLink->getLink();
                  break;
                default:
                  $person->websiteUri = $externalLink->getLink();
                  break;
              }
            }
          }
          break;
        case 'addresses':
          if(isset($value)) {
            foreach ($value['edges'] as $addressNode){
              $locality = Locality::fromResponse($addressNode);

              switch ($locality->getName()) {
                case 'birth':
                  $person->placeOfBirth = $locality;
                  break;
                case 'work':
                  $person->placeOfWork = $locality;
                  break;
              }
            }
          }
          break;
        default:
          $person->{$property} = $value;
      }
    }

    return $person;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    $parentJSON = parent::jsonSerialize();

    return array_merge($parentJSON,[
      'id' => $this->getId(),
      'firstName' => $this->getFirstName(),
      'lastName' => $this->getLastName(),
      'displayName' => $this->getFirstName() . " " . $this->getLastName(),
      'gender' => $this->getGender(),
      'bio' => $this->getBio(),
      'shortBio' => $this->getShortBio(),
      'birthday' => $this->getBirthday(),
      'placeOfBirth' => $this->getPlaceOfBirth() ? $this->getPlaceOfBirth()->jsonSerialize() : null,
      'placeOfWork' =>  $this->getPlaceOfWork()  ? $this->getPlaceOfWork()->jsonSerialize() : null,
    ]);
  }
}

<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;

class Involvement extends ModelAbstract {
  /** @var \ReSourceAdapter\Model\Actor Involvement actor */
  protected $actor;

  /** @var  string Involvement role */
  protected $role;

  /**
   * @return \ReSourceAdapter\Model\Actor
   */
  public function getActor() {
    return $this->actor;
  }

  /**
   * @return string
   */
  public function getRole() {
    return $this->role;
  }

  /**
   * Get organization GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $actorFragmentName =  Fragment::generateName();
    $actorFragment = Actor::getFragment($actorFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on Involvement{
  id
  seeAlso
  role
  agent{
    ...$actorFragmentName
  }
}

$actorFragment
GRAPHQL;
  }


  /**
   * Get organisation from GraphQL response data.
   * @param $data
   * @return \ReSourceAdapter\Model\Involvement
   */
  static function fromResponse($data) {
    $data = $data['involvement'];

    $involvement = new Involvement();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'agent':
          $involvement->actor = Actor::fromResponse(['agent' => $value]);
          break;

        default:
          $involvement->{$property} = $value;
      }
    }

    return $involvement;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'role' => $this->getRole(),
      'actor' => $this->getActor()->jsonSerialize()
    ];
  }
}

<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Cursor;
use ReSourceAdapter\Helpers\Fragment;
use ReSourceAdapter\Helpers\ListQueryParams;

abstract class Actor extends ModelAbstract {
  /** @var string Actor name */
  protected $displayName;

  /** @var  string Avatar image Uri */
  protected $avatar;

  /** @var string Actor facebook Uri */
  protected $facebookUri;

  /** @var string Actor twitter Uri */
  protected $twitterUri;

  /** @var string Actor instagram Uri */
  protected $instagramUri;

  /** @var string Actor website Uri */
  protected $websiteUri;

  /**
   * Get agent name
   * @return mixed
   */
  public function getDisplayName() {
    return $this->displayName;
  }

  /**
   * Get agent avatar
   * @return string
   */
  public function getAvatar() {
    return $this->avatar;
  }

  /**
   * @return string
   */
  public function getFacebookUri() {
    return $this->facebookUri;
  }

  /**
   * @return string
   */
  public function getTwitterUri() {
    return $this->twitterUri;
  }

  /**
   * @return string
   */
  public function getInstagramUri() {
    return $this->instagramUri;
  }

  /**
   * @return string
   */
  public function getWebsiteUri() {
    return $this->websiteUri;
  }

  /**
   * Get person GraphQL fragment.
   *
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $personFragmentName = Fragment::generateName();
    $personFragment = Person::getFragment($personFragmentName);

    $orgFragmentName = Fragment::generateName();
    $orgFragment = Organization::getFragment($orgFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on AgentInterface{
    __typename
    id
    displayName
    avatar
    seeAlso 
    creationDate: createdAt
    lastUpdate: updatedAt
    ...$personFragmentName
    ...$orgFragmentName
}

$personFragment
$orgFragment
GRAPHQL;
  }

  /**
   * Get organization GraphQL query.
   *
   * @param $agentId
   * @return string
   */
  static function getQuery($agentId){
    $fragmentName = Fragment::generateName();
    $fragment = self::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  agent(id: "$agentId") {
    ...$fragmentName
  }
}

$fragment
GRAPHQL;
  }

  /**
   * Get agents list GraphQL query.
   *
   * @param \ReSourceAdapter\Helpers\ListQueryParams $args
   * @return string
   */
  static function getListQuery(ListQueryParams $args){
    $fragmentName = Fragment::generateName();
    $fragment = self::getFragment($fragmentName);

    $pageInfo = Cursor::getPageInfoFragment();

    return <<<GRAPHQL
query{
  agents({$args->graphQLize()}) {
    edges{
      agent: node{
        ...$fragmentName
      }
    }
    $pageInfo
  }
}

$fragment

GRAPHQL;
  }

  /**
   * Return a list of Actor from a GraphQL response.
   *
   * @param array $data
   * @return \ReSourceAdapter\Model\Actor[]
   */
  static function fromListResponse(array $data){
    $agents = [];

    foreach ($data['agents']['edges'] as $agentData) {
      $agents[] = self::fromResponse($agentData);
    }

    return $agents;
  }

  /**
   * Return an agent from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Actor
   */
  static function fromResponse($data) {
    $data = $data['agent'];
    switch ($data['__typename']) {
      case 'Person':
        return Person::fromResponse(['person' => $data]);
      case 'Organization':
        return Organization::fromResponse(['organization' => $data]);
    }
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    return [
      'id' => $this->getId(),
      'displayName' => $this->getDisplayName(),
      'avatar' => $this->getAvatar(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate(),
      'facebookUri' => $this->getFacebookUri(),
      'twitterUri' => $this->getTwitterUri(),
      'instagramUri' => $this->getInstagramUri(),
      'websiteUri' => $this->getWebsiteUri()
    ];
  }
}

<?php
/**
 * This file is part of the lafayette-anticipations package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2017
 */

namespace ReSourceAdapter\Tests;

use PHPUnit\Framework\TestCase;
use ReSourceAdapter\Adapter;
use ReSourceAdapter\Model\Organization;
use ReSourceAdapter\Model\Person;
use ReSourceAdapter\Resolver\Client;

class AdapterTest extends TestCase {
  /** @var Adapter */
  protected $adapter;

  public function setUp() {
    $this->adapter = $this->getMockBuilder('ReSourceAdapter\Adapter')
      ->disableOriginalConstructor()
      ->setMethods(['__construct'])
      ->getMock();
  }

  public function tearDown() {
    unset($this->adapter);
  }

  /**
   */
  public function testFindActors(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
 {
    "actors": {
      "edges": [
        {
          "actor": {
            "__typename": "Person",
            "id": "UGVyc29uOiM2NTow",
            "displayName": "Mathieu Rogelja",
            "avatar": "http://zoo",
            "firstName": "Mathieu",
            "lastName": "Rogelja",
            "bio": "Un gars cool",
            "gender": "male"
          }
        },
        {
          "actor": {
            "__typename": "Organisation",
            "id": "T3JnYW5pc2F0aW9uOiM3Mzow",
            "displayName": "Mnemotix",
            "avatar": null,
            "description": "Smartup",
            "shortName": "MNX",
            "members": {
            "edges": [
                {
                  "affiliation": {
                    "id": "QWZmaWxpYXRpb246IzE0NTow",
                    "role": "JS Guru",
                    "person": {
                      "id": "UGVyc29uOiM2NTow",
                      "displayName": "Mathieu Rogelja",
                      "avatar": "http://zoo",
                      "firstName": "Mathieu",
                      "lastName": "Rogelja",
                      "bio": "Un gars cool",
                      "gender": "male"
                    }
                  }
                }
              ]
            }
          }
        }
      ],
      "pageInfo": {
        "hasNextPage": true,
        "hasPreviousPage": false,
        "startCursor": "YXJyYXljb25uZWN0aW9uOjA=",
        "endCursor": "YXJyYXljb25uZWN0aW9uOjU="
      }
    }
  }
JSON
      , JSON_OBJECT_AS_ARRAY));


    $actors = $this->adapter->findActors('', 2);
    $this->assertCount(2, $actors);
    $this->assertInstanceOf('ReSourceAdapter\Model\Actor', $actors[0]);
    $this->assertInstanceOf('ReSourceAdapter\Model\Person', $actors[0]);
    $this->assertInstanceOf('ReSourceAdapter\Model\Organization', $actors[1]);


    /** @var Person $person */
    $person = $actors[0];
    $this->assertEquals('UGVyc29uOiM2NTow', $person->getId());
    $this->assertEquals('Mathieu Rogelja', $person->getDisplayName());
    $this->assertEquals('Un gars cool', $person->getBio());
    $this->assertEquals('male', $person->getGender());
    $this->assertEquals('http://zoo', $person->getAvatar());

    /** @var Organization $org */
    $org = $actors[1];

    $this->assertEquals('T3JnYW5pc2F0aW9uOiM3Mzow', $org->getId());
    $this->assertEquals('Mnemotix', $org->getDisplayName());
    $this->assertEquals('Smartup', $org->getDescription());
    $this->assertEquals('MNX', $org->getShortName());
    $this->assertCount(1, $org->getMembers());

    $this->assertEquals('JS Guru', $org->getMembers()[0]->getRole());
    $this->assertInstanceOf('ReSourceAdapter\Model\Person', $org->getMembers()[0]->getPerson());

    $person = $org->getMembers()[0]->getPerson();

    $this->assertEquals('UGVyc29uOiM2NTow', $person->getId());
    $this->assertEquals('Mathieu Rogelja', $person->getDisplayName());
    $this->assertEquals('Un gars cool', $person->getBio());
    $this->assertEquals('male', $person->getGender());
    $this->assertEquals('http://zoo', $person->getAvatar());
  }

  public function testFindExhibitions(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "exhibitions": {
    "edges": [
      {
        "exhibition": {
          "id": "UHJvamVjdDojODE6MA==",
          "title": "Exposition permanente",
          "description": "Une grande première",
          "authors": {
            "edges": [
              {
                "actor": {
                  "__typename": "Person",
                  "id": "UGVyc29uOiM2NTow",
                  "displayName": "Mathieu Rogelja",
                  "avatar": null,
                  "firstName": "Mathieu",
                  "lastName": "Rogelja",
                  "bio": null,
                  "gender": null
                }
              }
            ]
          },
          "project":{
            "involvements": {
              "edges": [
                {
                  "involvement": {
                    "id": "SW52b2x2ZW1lbnQ6IzEyMjow",
                    "role": "Star",
                    "actor": {
                      "__typename": "Person",
                      "id": "UGVyc29uOiM2NTow",
                      "displayName": "Mathieu Rogelja",
                      "avatar": null,
                      "firstName": "Mathieu",
                      "lastName": "Rogelja",
                      "bio": "Un gars cool",
                      "gender": null
                    }
                  }
                }
              ]
            }
          }
        }
      }
    ],
    "pageInfo": {
      "hasNextPage": false,
      "hasPreviousPage": false,
      "startCursor": "YXJyYXljb25uZWN0aW9uOjA=",
      "endCursor": "YXJyYXljb25uZWN0aW9uOjA="
    }
  }
}
JSON
, JSON_OBJECT_AS_ARRAY));

    $exhibitions = $this->adapter->findExhibitions('', 1);
    $this->assertCount(1, $exhibitions);


    $this->assertInstanceOf('ReSourceAdapter\Model\Exhibition', $exhibitions[0]);

    $this->assertEquals('Exposition permanente', $exhibitions[0]->getTitle());
    $this->assertEquals('Une grande première', $exhibitions[0]->getDescription());

    $this->assertCount(1, $exhibitions[0]->getAuthors());
    $this->assertInstanceOf('ReSourceAdapter\Model\Person',  $exhibitions[0]->getAuthors()[0]);
    $this->assertEquals("Mathieu Rogelja",  $exhibitions[0]->getAuthors()[0]->getDisplayName());

    $this->assertCount(1, $exhibitions[0]->getInvolvements());
    $this->assertInstanceOf('ReSourceAdapter\Model\Involvement',  $exhibitions[0]->getInvolvements()[0]);
    $this->assertEquals("Star",  $exhibitions[0]->getInvolvements()[0]->getRole());

    $this->assertInstanceOf('ReSourceAdapter\Model\Actor',  $exhibitions[0]->getInvolvements()[0]->getActor());

    $this->assertEquals("Mathieu Rogelja",  $exhibitions[0]->getInvolvements()[0]->getActor()->getDisplayName());
  }

  public function testFindExhibitionArtworks(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "exhibition": {
    "id": "UHJvamVjdDojODE6MA==",
    "title": "Exposition permanente",
    "description": "Une grande première",
    "artworks": {
      "edges": [
        {
          "artwork": {
            "id": "UHJvamVjdDojODI6MA==",
            "title": "Oeuvre 1",
            "description": "Une oeuvre vraiment belle.",
            "authors": {
              "edges": [
                {
                  "actor": {
                    "__typename": "Person",
                    "id": "UGVyc29uOiM2NTow",
                    "displayName": "Mathieu Rogelja",
                    "avatar": null,
                    "firstName": "Mathieu",
                    "lastName": "Rogelja",
                    "bio": null,
                    "gender": null
                  }
                }
              ]
            },
            "project": {
               "involvements": {
                "edges": [
                  {
                    "involvement": {
                      "id": "SW52b2x2ZW1lbnQ6IzEyMjow",
                      "role": "Star",
                      "actor": {
                        "__typename": "Person",
                        "id": "UGVyc29uOiM2NTow",
                        "displayName": "Mathieu Rogelja",
                        "avatar": null,
                        "firstName": "Mathieu",
                        "lastName": "Rogelja",
                        "bio": "Un gars cool",
                        "gender": null
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      ]
    }
  }
}
JSON
      , JSON_OBJECT_AS_ARRAY));

    $artworks = $this->adapter->findExhibitionArtworks("UHJvamVjdDojODE6MA==", '', 1);

    $this->assertCount(1, $artworks);

    $this->assertInstanceOf('ReSourceAdapter\Model\Artwork', $artworks[0]);
    $this->assertEquals('Oeuvre 1', $artworks[0]->getTitle());
    $this->assertEquals('Une oeuvre vraiment belle.', $artworks[0]->getDescription());

    $this->assertCount(1, $artworks[0]->getAuthors());
    $this->assertInstanceOf('ReSourceAdapter\Model\Person',  $artworks[0]->getAuthors()[0]);
    $this->assertEquals("Mathieu Rogelja",  $artworks[0]->getAuthors()[0]->getDisplayName());
  }

  public function testFindProjectEvents(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{ 
  "artisticObject": {
   "project": {
    "events": {
      "edges": [
        {
          "event": {
            "id": "RXZlbnQ6IzkwOjA=",
            "title": "Un premier évènement",
            "description": "Waou !",
            "endDate": 1493211459803,
            "startDate": 1493211468350,
            "involvements": {
              "edges": [
                {
                  "involvement": {
                    "id": "SW52b2x2ZW1lbnQ6IzEyMzow",
                    "role": "Super star",
                    "actor": {
                      "__typename": "Person",
                      "id": "UGVyc29uOiM2NTow",
                      "displayName": "Mathieu Rogelja",
                      "avatar": null,
                      "firstName": "Mathieu",
                      "lastName": "Rogelja",
                      "bio": "Un gars cool",
                      "gender": null
                    }
                  }
                }
              ]
            },
            "attachments": {
              "edges": [
                {
                  "attachment": {
                    "resource": {
                      "id": "UmVzb3VyY2U6IzQ5OjA=",
                      "title": "Resource 1",
                      "description": "Description de resource 1",
                      "uri": "http://localhost:8082/s/dArVRcDpeS34zuj/download",
                      "credits": "© Labo photo"
                    }
                  }
                }
              ]
            }
          }
        }
      ]
    }
  }
  }
}
JSON
      , JSON_OBJECT_AS_ARRAY));

    $events = $this->adapter->findExhibitionEvents("UHJvamVjdDojODE6MA==", '', 1);


    $this->assertCount(1, $events);

    $this->assertInstanceOf('ReSourceAdapter\Model\Event', $events[0]);
    $this->assertEquals('Un premier évènement', $events[0]->getTitle());
    $this->assertEquals('Waou !', $events[0]->getDescription());

    $this->assertCount(1, $events[0]->getInvolvements());
    $this->assertInstanceOf('ReSourceAdapter\Model\Involvement',  $events[0]->getInvolvements()[0]);
    $this->assertEquals("Super star",  $events[0]->getInvolvements()[0]->getRole());

    $this->assertInstanceOf('ReSourceAdapter\Model\Actor',  $events[0]->getInvolvements()[0]->getActor());
    $this->assertEquals("Mathieu Rogelja",  $events[0]->getInvolvements()[0]->getActor()->getDisplayName());

    $this->assertCount(1, $events[0]->getResources());
    $this->assertInstanceOf('ReSourceAdapter\Model\Resource',  $events[0]->getResources()[0]);

    $this->assertEquals("Resource 1",  $events[0]->getResources()[0]->getTitle());
    $this->assertEquals("Description de resource 1",  $events[0]->getResources()[0]->getDescription());
    $this->assertEquals("UmVzb3VyY2U6IzQ5OjA=",  $events[0]->getResources()[0]->getId());
    $this->assertEquals("© Labo photo",  $events[0]->getResources()[0]->getCredits());
  }

  public function testGetActorAsPerson(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "actor": {
    "__typename": "Person",
    "id": "UGVyc29uOiM2NTow",
    "displayName": "Mathieu Rogelja",
    "avatar": "http://zoo",
    "firstName": "Mathieu",
    "lastName": "Rogelja",
    "bio": "Un gars cool",
    "gender": "male"
  }
}
JSON
    , JSON_OBJECT_AS_ARRAY));

    $actor = $this->adapter->getActor("UGVyc29uOiM2NTow");

    $this->assertInstanceOf('ReSourceAdapter\Model\Actor', $actor);

    /** @var Person $person */
    $person = $actor;

    $this->assertEquals('UGVyc29uOiM2NTow', $person->getId());
    $this->assertEquals('Mathieu Rogelja', $person->getDisplayName());
    $this->assertEquals('Un gars cool', $person->getBio());
    $this->assertEquals('male', $person->getGender());
    $this->assertEquals('http://zoo', $person->getAvatar());
  }

  public function testGetActorAsOrg(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "actor": {
    "__typename": "Organisation",
    "id": "T3JnYW5pc2F0aW9uOiM3Mzow",
    "displayName": "Mnemotix",
    "avatar": null,
    "description": "Smartup",
    "shortName": "MNX",
    "members": {
    "edges": [
        {
          "affiliation": {
            "id": "QWZmaWxpYXRpb246IzE0NTow",
            "role": "JS Guru",
            "person": {
              "id": "UGVyc29uOiM2NTow",
              "displayName": "Mathieu Rogelja",
              "avatar": "http://zoo",
              "firstName": "Mathieu",
              "lastName": "Rogelja",
              "bio": "Un gars cool",
              "gender": "male"
            }
          }
        }
      ]
    }
  }
}
JSON
      , JSON_OBJECT_AS_ARRAY));

    $actor = $this->adapter->getActor("T3JnYW5pc2F0aW9uOiM3Mzow");

    /** @var Organization $org */
    $org = $actor;

    $this->assertEquals('T3JnYW5pc2F0aW9uOiM3Mzow', $org->getId());
    $this->assertEquals('Mnemotix', $org->getDisplayName());
    $this->assertEquals('Smartup', $org->getDescription());
    $this->assertEquals('MNX', $org->getShortName());
    $this->assertCount(1, $org->getMembers());

    $this->assertEquals('JS Guru', $org->getMembers()[0]->getRole());
    $this->assertInstanceOf('ReSourceAdapter\Model\Person', $org->getMembers()[0]->getPerson());

    $person = $org->getMembers()[0]->getPerson();

    $this->assertEquals('UGVyc29uOiM2NTow', $person->getId());
    $this->assertEquals('Mathieu Rogelja', $person->getDisplayName());
    $this->assertEquals('Un gars cool', $person->getBio());
    $this->assertEquals('male', $person->getGender());
    $this->assertEquals('http://zoo', $person->getAvatar());
  }

  public function testGetExhibition(){

    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "exhibition": {
    "id": "UHJvamVjdDojODE6MA==",
    "title": "Exposition permanente",
    "description": "Une grande première",
    "authors": {
      "edges": [
        {
          "actor": {
            "__typename": "Person",
            "id": "UGVyc29uOiM2NTow",
            "displayName": "Mathieu Rogelja",
            "avatar": null,
            "firstName": "Mathieu",
            "lastName": "Rogelja",
            "bio": null,
            "gender": null
          }
        }
      ]
    },
    "project": {
      "involvements": {
        "edges": [
          {
            "involvement": {
              "id": "SW52b2x2ZW1lbnQ6IzEyMjow",
              "role": "Star",
              "actor": {
                "__typename": "Person",
                "id": "UGVyc29uOiM2NTow",
                "displayName": "Mathieu Rogelja",
                "avatar": null,
                "firstName": "Mathieu",
                "lastName": "Rogelja",
                "bio": "Un gars cool",
                "gender": null
              }
            }
          }
        ]
      }
    }
    
  }
  }
JSON
      , JSON_OBJECT_AS_ARRAY));

    $exhibition = $this->adapter->getExhibition("UHJvamVjdDojODE6MA==");

    $this->assertInstanceOf('ReSourceAdapter\Model\Exhibition', $exhibition);

    $this->assertEquals('Exposition permanente', $exhibition->getTitle());
    $this->assertEquals('Une grande première', $exhibition->getDescription());

    $this->assertCount(1, $exhibition->getAuthors());
    $this->assertInstanceOf('ReSourceAdapter\Model\Person',  $exhibition->getAuthors()[0]);
    $this->assertEquals("Mathieu Rogelja",  $exhibition->getAuthors()[0]->getDisplayName());

    $this->assertCount(1, $exhibition->getInvolvements());
    $this->assertInstanceOf('ReSourceAdapter\Model\Involvement',  $exhibition->getInvolvements()[0]);
    $this->assertEquals("Star",  $exhibition->getInvolvements()[0]->getRole());

    $this->assertInstanceOf('ReSourceAdapter\Model\Actor',  $exhibition->getInvolvements()[0]->getActor());

    $this->assertEquals("Mathieu Rogelja",  $exhibition->getInvolvements()[0]->getActor()->getDisplayName());
  }

  public function testGetArtwork(){

    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "artwork": {
    "id": "UHJvamVjdDojODI6MA==",
    "title": "Oeuvre 1",
    "description": "Une oeuvre vraiment belle.",
    "authors": {
      "edges": [
        {
          "actor": {
            "__typename": "Person",
            "id": "UGVyc29uOiM2NTow",
            "displayName": "Mathieu Rogelja",
            "avatar": null,
            "firstName": "Mathieu",
            "lastName": "Rogelja",
            "bio": null,
            "gender": null
          }
        }
      ]
    }
  }
}
JSON
      , JSON_OBJECT_AS_ARRAY));

    $artwork = $this->adapter->getArtwork("UHJvamVjdDojODI6MA==");

    $this->assertInstanceOf('ReSourceAdapter\Model\Artwork', $artwork);
    $this->assertEquals('Oeuvre 1', $artwork->getTitle());
    $this->assertEquals('Une oeuvre vraiment belle.', $artwork->getDescription());

    $this->assertCount(1, $artwork->getAuthors());
    $this->assertInstanceOf('ReSourceAdapter\Model\Person',  $artwork->getAuthors()[0]);
    $this->assertEquals("Mathieu Rogelja",  $artwork->getAuthors()[0]->getDisplayName());
  }

  public function testGetEvent(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "event": {
    "id": "RXZlbnQ6IzkwOjA=",
    "title": "Un premier évènement",
    "description": "Waou !",
    "endDate": 1493211459803,
    "startDate": 1493211468350,
    "involvements": {
      "edges": [
        {
          "involvement": {
            "id": "SW52b2x2ZW1lbnQ6IzEyMzow",
            "role": "Super star",
            "actor": {
              "__typename": "Person",
              "id": "UGVyc29uOiM2NTow",
              "displayName": "Mathieu Rogelja",
              "avatar": null,
              "firstName": "Mathieu",
              "lastName": "Rogelja",
              "bio": "Un gars cool",
              "gender": null
            }
          }
        }
      ]
    },
    "attachments": {
      "edges": [
        {
          "attachment": {
            "resource": {
              "id": "UmVzb3VyY2U6IzQ5OjA=",
              "title": "Resource 1",
              "description": "Description de resource 1",
              "uri": "http://localhost:8082/s/dArVRcDpeS34zuj/download",
              "credits": "© Labo photo"
            }
          }
        }
      ]
    }
  }
  }
JSON
      , JSON_OBJECT_AS_ARRAY));

    $event = $this->adapter->getEvent("RXZlbnQ6IzkwOjA");

    $this->assertInstanceOf('ReSourceAdapter\Model\Event', $event);
    $this->assertEquals('Un premier évènement', $event->getTitle());
    $this->assertEquals('Waou !', $event->getDescription());

    $this->assertCount(1, $event->getInvolvements());
    $this->assertInstanceOf('ReSourceAdapter\Model\Involvement',  $event->getInvolvements()[0]);
    $this->assertEquals("Super star",  $event->getInvolvements()[0]->getRole());

    $this->assertInstanceOf('ReSourceAdapter\Model\Actor',  $event->getInvolvements()[0]->getActor());
    $this->assertEquals("Mathieu Rogelja",  $event->getInvolvements()[0]->getActor()->getDisplayName());

    $this->assertCount(1, $event->getResources());
    $this->assertInstanceOf('ReSourceAdapter\Model\Resource',  $event->getResources()[0]);

    $this->assertEquals("Resource 1",  $event->getResources()[0]->getTitle());
    $this->assertEquals("Description de resource 1",  $event->getResources()[0]->getDescription());
    $this->assertEquals("UmVzb3VyY2U6IzQ5OjA=",  $event->getResources()[0]->getId());
    $this->assertEquals("© Labo photo",  $event->getResources()[0]->getCredits());
  }

  /**
   */
  public function testGetResource(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "resource": {
    "id": "UmVzb3VyY2U6IzQ5OjA=",
    "title": "Resource 1",
    "description": "Description de resource 1",
    "uri": "http://localhost:8082/s/dArVRcDpeS34zuj/download",
    "credits": "© Labo photo"
  }
}
JSON
      , JSON_OBJECT_AS_ARRAY));

    $resource = $this->adapter->getResource("UmVzb3VyY2U6IzQ5OjA=");

    $this->assertInstanceOf('ReSourceAdapter\Model\Resource', $resource);

    $this->assertEquals("Resource 1",  $resource->getTitle());
    $this->assertEquals("Description de resource 1",  $resource->getDescription());
    $this->assertEquals("UmVzb3VyY2U6IzQ5OjA=",  $resource->getId());
    $this->assertEquals("© Labo photo",  $resource->getCredits());

  }

  /**
   */
  public function testGetResourcesDifferentialSince(){
    $clientStub = $this->createMock(Client::class);
    $this->adapter->setClient($clientStub);

    $clientStub->method('resolveQuery')->willReturn(json_decode(<<<JSON
{
  "resourcesDiff": {
    "edges": [
      {
        "resourceActivity": {
          "resource": {
            "id": "UmVzb3VyY2U6IzUyOjA=",
            "title": "Nouvel évènement le 16 mai 2017",
            "description": "Test 2 images",
            "credits": null,
            "uri": "http://localhost:8082/s/s8DDixTyDzLXBcB/download"
          },
          "action": "CREATED"
        }
      },
      {
        "resourceActivity": {
          "resource": {
            "id": "UmVzb3VyY2U6IzQ5OjQ=",
            "title": "Evenement 1",
            "description": null,
            "credits": null,
            "uri": "http://localhost:8082/s/zmA3K5f9JPksEO0/download"
          },
          "action": "CREATED"
        }
      }
    ]
  }
}
JSON
      , JSON_OBJECT_AS_ARRAY));

    $resourceActivities = $this->adapter->getResourcesDifferentialSince(1, 2);

    $this->assertCount(2, $resourceActivities);
    $this->assertInstanceOf('ReSourceAdapter\Model\ResourceActivity', $resourceActivities[0]);

    $this->assertInstanceOf('ReSourceAdapter\Model\Resource',  $resourceActivities[0]->getResource());
    $this->assertEquals("CREATED",  $resourceActivities[0]->getAction());
  }
}

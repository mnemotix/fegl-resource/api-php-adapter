<?php
/**
 * This file is part of the re-source-drupal-adapter package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 11/05/2017
 */

require "../vendor/autoload.php";

use ReSourceAdapter\Adapter;

$adapter = new Adapter();

$qs = "";
$first = 10;
$after = null;

if (isset($_GET["qs"])) {
  $qs = $_GET["qs"];
}

if (isset($_GET["first"])) {
  $first = (int) $_GET["first"];
}

if (isset($_GET["after"])) {
  $after = (int) $_GET["after"];
}

$exhibitions = $adapter->findEventResources($_GET["id"], $qs, $first, $after);

header("Content-type:application/json");
echo json_encode($exhibitions, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
